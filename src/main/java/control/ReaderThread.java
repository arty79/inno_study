package control;


import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ReaderThread extends Thread {

    private RandomSecundomer timer;

    public ReaderThread(RandomSecundomer t) {
        this.timer = t;
    }

    @Override
    public void run() {
        while (ControlTask.isContinue()) {
            synchronized (timer) {
                try {
                    if (timer.getTime() % 5 == 0 && timer.getTime() != 0) {
                        System.out.println("tic 5 sec");
                            printResults();
                    }
                    timer.wait();

                } catch (InterruptedException e) {
                }
            }
        }
    }



    private void printResults() {
        for (Map.Entry<Integer, Integer> pair : ControlTask.getResults().entrySet()) {
            System.out.println(pair.getKey() + " количество: " + pair.getValue());
        }
    }
}
