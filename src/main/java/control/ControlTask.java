package control;

import java.util.concurrent.ConcurrentHashMap;

public class ControlTask {

    private static ConcurrentHashMap<Integer, Integer> results = new ConcurrentHashMap<>();

    private static volatile boolean isContinue = true;

    public static boolean isContinue() {
        return isContinue;
    }

    public static void setIsContinue(boolean isContinue) {
        ControlTask.isContinue = isContinue;
    }

    public static ConcurrentHashMap<Integer, Integer> getResults() {
        return results;
    }


    public static void main(String[] args) {
        RandomSecundomer t = new RandomSecundomer();
        Thread t2 = new ReaderThread(t);
        t.start();
        t2.start();
    }
}
