package control;

import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

public class RandomSecundomer extends Thread {

    private static Date startDate = new Date();
    private static long time;
    private Random r = new Random();
    private int random = 0;

    public Date getStartDate() {
        return startDate;
    }

    public static long getTime() {
        return time;
    }

    @Override
    public void run() {
        ConcurrentHashMap<Integer, Integer> results = ControlTask.getResults();
        while (ControlTask.isContinue() && countQuantity(results)) {
            synchronized (this) {
                int temp = 0;
                random = r.nextInt(10);
                if (results.containsKey(random)) {
                    temp = results.get(random);
                    results.put(random, ++temp);
                } else {
                    results.put(random, 1);
                }
                time = (new Date().getTime() - startDate.getTime()) / 1000;
                notifyAll();
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean countQuantity(ConcurrentHashMap<Integer, Integer> results) {
        for (Map.Entry<Integer, Integer> pair : results.entrySet()) {
            if (pair.getValue() > 5) {
                ControlTask.setIsContinue(false);
                return false;
            }
        }
        return true;
    }
}
