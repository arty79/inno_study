package library.logger;

import com.loader.Book;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.spi.LoggingEvent;

public class CustomLogLayout extends PatternLayout {

    public String format(LoggingEvent event) {
        Book book = (Book) event.getMessage();
        StringBuffer sb = new StringBuffer();
        String author = book.getAuthor();
        String title = book.getTitle();
        int year = book.getYear();
        String isbn = book.getIsbn();

        sb.append("<book>");
        sb.append("<author>").append(author).append("</author>");
        sb.append("<title>").append(title).append("</title>");
        sb.append("<year>").append(year).append("</year>");
        sb.append("<isbn>").append(isbn).append("</isbn>");
        sb.append("</book>");
        sb.append("\n");
        return sb.toString();
    }
}
