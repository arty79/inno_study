package library.logger;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.spi.LoggingEvent;

public class CustomAppender extends AppenderSkeleton {

    @Override
    public void close() {
    }

    @Override
    public boolean requiresLayout() {
        return false;
    }

    @Override
    protected void append(LoggingEvent event) {

        try {
            //StringBuilder sb =new StringBuilder();
            //sb.append(event.getLevel().toString());
            //sb.append(event.getMessage().toString());
            EmailSender.SendMsg(layout.format(event));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

