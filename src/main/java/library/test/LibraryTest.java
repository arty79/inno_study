package library.test;

import library.Library;
import com.loader.Book;
import library.modules.BookInstance;
import library.modules.Reader;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by admin on 10.04.2017.
 */
public class LibraryTest {

    private static Library library;

    @Before
    public void init() {
        library = new Library();
    }

    @Test
    public void buyBookTest() throws Exception {
        Book newBook = new Book("Shildt", "Intro to Java", 2017, "1212124454adfa");
        library.buyBook("Shildt", "Intro to Java", "1212124454adfa", 5, 2017);

        assertEquals(1, library.getCatalog().size());
        assertTrue(library.getCatalog().contains(newBook));
        Book bookFromBook = library.getCatalog().iterator().next();
        assertTrue(newBook.getTitle().equals(bookFromBook.getTitle()));
        assertTrue(newBook.getAuthor().equals(bookFromBook.getAuthor()));
        assertTrue(newBook.getIsbn().equals(bookFromBook.getIsbn()));
        assertTrue(newBook.getYear() == bookFromBook.getYear());
    }


    @Test
    public void buyBookTestStore() {
        Book newBook = new Book("Intro to Java", "Shildt", 2017, "1212124454adfa");
        library.buyBook("Intro to Java", "Shildt", "1212124454adfa", 5, 2017);
        assertEquals(5, library.getStore().size());
        for (BookInstance instance :
                library.getStore()) {
            Book bookFromBook = instance.getBook();
            assertTrue(newBook.getTitle().equals(bookFromBook.getTitle()));
            assertTrue(newBook.getAuthor().equals(bookFromBook.getAuthor()));
            assertTrue(newBook.getIsbn().equals(bookFromBook.getIsbn()));
            assertTrue(newBook.getYear() == bookFromBook.getYear());
        }
    }

    @Test
    public void takeBook() throws Exception {
        Book newBook = new Book("Shildt", "Intro to Java", 2017, "1212124454adfa");
        library.buyBook("Shildt", "Intro to Java", "1212124454adfa", 5, 2017);
        int beforeCount = getQuantity(newBook);
        library.takeBook("Ivan", "Ivanovich", "Ivanov", 123456, "Intro to Java");
        Reader newReader = new Reader("Ivan", "Ivanovich", "Ivanov", 123456);
        assertTrue(library.getReaders().contains(newReader));
        assertEquals(beforeCount - 1, getQuantity(newBook));
    }

    public int getQuantity(Book newBook) {
        int count = 0;
        for (BookInstance bookInstance : library.getStore()
                ) {
            if (bookInstance.getBook().equals(newBook)) {
                count++;
            }
        }
        return count;
    }

    @Test
    public void returnBook() throws Exception {

        Book newBook = new Book("Shildt", "Intro to Java", 2017, "1212124454adfa");
        library.buyBook("Shildt", "Intro to Java", "1212124454adfa", 5, 2017);

        int beforeCount = getQuantity(newBook);

        library.takeBook("Ivan", "Ivanovich", "Ivanov", 123456, "Intro to Java");
        library.returnBook("Ivan", "Ivanovich", "Ivanov", 123456, "Intro to Java");
        Reader newReader = new Reader("Ivan", "Ivanovich", "Ivanov", 123456);
        assertTrue(library.getReaders().contains(newReader));
        assertEquals(beforeCount, getQuantity(newBook));
    }

}
