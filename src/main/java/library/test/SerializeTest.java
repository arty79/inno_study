package library.test;

import com.loader.Book;
import library.utils.DataManager;
import library.utils.MySerializable;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Proxy;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;

import static junit.framework.TestCase.assertEquals;


public class SerializeTest {

    private DataManager dataManager;

    @Test
    public void testSerializationBook() {

        Book book = new Book("Shildt", "Intro to Java", 2017, "12121332");

        DataManager dataManager = new DataManager();

        MySerializable proxySerializer = (MySerializable) Proxy.newProxyInstance(
                DataManager.class.getClassLoader(),
                DataManager.class.getInterfaces(),
                new ProxySerializer(dataManager));

        File proxyFile = proxySerializer.serializeOneBookToFile(book, "proxy_test.txt");

        File file = dataManager.serializeOneBookToFile(book, "test.txt");

        assertEquals(doChecksum(proxyFile.getName()), doChecksum(file.getName()));
    }

    @Test
    public void testDeserializationBook() {

        Book book = new Book("Shildt", "Intro to Java", 2017, "12121332");

        DataManager dataManager = new DataManager();

        MySerializable proxySerializer = (MySerializable) Proxy.newProxyInstance(
                DataManager.class.getClassLoader(),
                DataManager.class.getInterfaces(),
                new ProxySerializer(dataManager));

        Book bookProxy = proxySerializer.deSerializeBookFromFile(new Book(), "proxy_test.txt");

        Book bookFromFile = dataManager.deSerializeBookFromFile(new Book(), "test.txt");

        assertEquals(bookProxy, book);
        assertEquals(bookFromFile, book);
    }

    private static long doChecksum(String fileName) {
        try {
            CheckedInputStream cis = null;
            long fileSize = 0;
            try {
                // Computer CRC32 checksum
                cis = new CheckedInputStream(
                        new FileInputStream(fileName), new CRC32());

                fileSize = new File(fileName).length();

            } catch (FileNotFoundException e) {
                System.err.println("File not found.");
                System.exit(1);
            }

            byte[] buf = new byte[128];
            while (cis.read(buf) >= 0) {
            }

            long checksum = cis.getChecksum().getValue();
            //System.out.println(checksum + " " + fileSize + " " + fileName);
            return checksum;
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return 0;
    }
}
