package library.test;

import com.loader.Book;
import library.utils.DataManager;

import java.io.*;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;


public class ProxySerializer implements InvocationHandler {

    private DataManager manager;

    public ProxySerializer(DataManager manager) {
        this.manager = manager;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("i'am proxy!");
        if (method.getName().equals("serializeOneBookToFile")) {
            try (FileOutputStream fos = new FileOutputStream(args[1].toString());
                 ObjectOutputStream oos = new ObjectOutputStream(fos)) {
                oos.writeObject(args[0]);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (method.getName().equals("deSerializeBookFromFile")) {
            Book obj = null;
            try (FileInputStream fis = new FileInputStream(args[1].toString());
                 ObjectInputStream ois = new ObjectInputStream(fis)) {
                obj = (Book) ois.readObject();
                return obj;
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return method.invoke(manager, args);
    }
}
