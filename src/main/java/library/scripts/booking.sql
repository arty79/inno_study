CREATE TABLE booking(
  bkg_id serial NOT NULL,
  rdr_reader_id integer NOT NULL,
  book_book_id integer NOT NULL,
  bkg_start_date date,
  bkg_return_date date,
  bkg_finish_date date,
  CONSTRAINT booking_pkey PRIMARY KEY (bkg_id),
  CONSTRAINT fk_booking_reader FOREIGN KEY (rdr_reader_id)
      REFERENCES reader (rdr_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_booking__book FOREIGN KEY (book_book_id)
      REFERENCES book (book_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
)