CREATE TABLE book(
  book_id serial NOT NULL,
  book_author character varying(255),
  book_title character varying(255),
  book_isbn character varying(255),
  book_year integer,
  CONSTRAINT book_pkey PRIMARY KEY (book_id)
)
WITH (
  OIDS=FALSE
);
