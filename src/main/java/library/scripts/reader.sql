CREATE TABLE reader(
  rdr_id serial NOT NULL,
  rdr_first_name character varying(255),
  rdr_second_name character varying(255),
  rdr_last_name character varying(255),
  rdr_passport_number character varying(255),
  CONSTRAINT rdr_pkey PRIMARY KEY (rdr_id)
)
WITH (
  OIDS=FALSE
);