CREATE TABLE book_instance(
  bin_id serial NOT NULL,
  bin_uuid uuid,
  book_book_id integer NOT NULL,
  CONSTRAINT bin_pkey PRIMARY KEY (bin_id),
  CONSTRAINT fk_book_instance__book FOREIGN KEY (book_book_id)
      REFERENCES book (book_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);