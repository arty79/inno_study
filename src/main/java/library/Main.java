package library;

import library.utils.JarClassLoader;
import java.lang.reflect.InvocationTargetException;
import java.util.stream.Stream;


public class Main {

    public static void main(String[] args) throws ClassNotFoundException {
        JarClassLoader classLoader = new JarClassLoader();
        Class bookClass = classLoader.loadClass("library.modules.Book");
        library.modules.Book bookInstance = null;
        try {
            bookInstance = (library.modules.Book) bookClass.newInstance();
            checkObject(bookClass, bookInstance);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        com.loader.Book fakeBookInstance = null;
        Class fakeBookClass = classLoader.loadClass("com.loader.Book");
        try {
            fakeBookInstance = (com.loader.Book) fakeBookClass.newInstance();
            checkObject(fakeBookClass, fakeBookInstance);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private static <T> void checkObject(Class bookClass, T bookInstance) {
        String str = null;
        try {
            str = (String) Stream.of(bookClass.getDeclaredMethods()).filter(m ->
                    m.getName().equals("toString"))
                    .findFirst().get()
                    .invoke(bookInstance);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        System.out.println(str);
    }
}

