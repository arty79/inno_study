package library.utils;

import java.io.*;
import java.util.HashSet;
import java.util.Set;

public class DataManager implements MySerializable {


    public static <T> void serializeToFile(Set<T> objects) {
        try (FileOutputStream fos = new FileOutputStream("books.txt");
             ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeInt(objects.size());
            for (T obj : objects
                    ) {
                oos.writeObject(obj);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public File serializeOneBookToFile(Object obj, String file) {
        try (FileOutputStream fos = new FileOutputStream(file);
             ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(obj);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new File(file);
    }

    public <T> T deSerializeBookFromFile(T t, String file) {
        T obj = null;
        try (FileInputStream fis = new FileInputStream(file);
             ObjectInputStream ois = new ObjectInputStream(fis)) {
             obj = (T) ois.readObject();
            return obj;
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return obj;
    }

    public static <T> Set<T> deserialize() {
        Set<T> objects = new HashSet<>();
        try (FileInputStream fis = new FileInputStream("books.txt");
             ObjectInputStream ois = new ObjectInputStream(fis)) {
            int count = ois.readInt();
            for (int i = 0; i < count; i++) {
                T obj = (T) ois.readObject();
                objects.add(obj);
            }
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return objects;
    }
}

