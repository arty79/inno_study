package library.utils;

import java.io.File;

public interface MySerializable {
    File serializeOneBookToFile(Object obj, String file);

    <T> T deSerializeBookFromFile(T t, String file);
}
