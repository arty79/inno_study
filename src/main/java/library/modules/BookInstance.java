package library.modules;

import com.loader.Book;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class BookInstance implements Externalizable{

    private static final long serialVersionUID = -228223910304329234L;
    private Book book;
    private UUID number;

    private List<Booking> bookingHistory;

    public BookInstance() {
    }

    public BookInstance(Book book, UUID number) {
        this.book = book;
        this.number = number;
        bookingHistory = new ArrayList<>(32);
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public List<Booking> getBookingHistory() {
        return bookingHistory;
    }

    public void setBookingHistory(List<Booking> bookingHistory) {
        this.bookingHistory = bookingHistory;
    }

    @Override
    public int hashCode() {
        return number.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof BookInstance)) {
            return false;
        }
        if (!this.number.equals(((BookInstance) obj).number)) return false;
        return true;
    }

    @Override
    public String toString() {
        return book + "@" + number;
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeObject(book);
        out.writeObject(number);
        out.writeUTF("Artem_bookInstance");
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        this.book = (Book) in.readObject();
        this.number = (UUID) in.readObject();
        System.out.println(in.readUTF());
    }
}
