package library.modules;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

public class Book {

    private String author;

    private String title;

    private int year;

    private String isbn;

    public Book() {
    }

    public void printClassInfo() {
        System.out.println(Book.class.getCanonicalName());
        for (Method method : this.getClass().getMethods()
                ) {
            System.out.println(method.getName());
            System.out.println(method.getReturnType().getName());
            for (Parameter parameter : method.getParameters()
                    ) {
                System.out.println(parameter.getName() + " " + parameter.getType().getName());
            }
            System.out.println(method.getModifiers());
        }
        try {
            for (Field field: Class.forName("library.modules.Book").getFields()
                 ) {
                System.out.println(field.getName());
                System.out.println(field.getType().getName());
                System.out.println(field.isAccessible());
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Book(String author, String title, int year, String isbn) {
        this.author = author;
        this.title = title;
        this.year = year;
        this.isbn = isbn;
    }

    private String serialize(Book book) {
        StringBuilder sb =new StringBuilder();
        sb.append(book.getAuthor().toString());
        sb.append(" ");
        sb.append(book.getTitle().toString());
        sb.append(" ");
        return sb.toString();
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public int getYear() {
        return year;
    }

    public String getIsbn() {
        return isbn;
    }


    @Override
    public int hashCode() {
        return isbn.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Book)) {
            return false;
        }
        if (!this.isbn.equals(((Book) obj).isbn)) return false;
        return true;
    }

    @Override
    public String toString() {
        return "Book{" +
                "author='" + author + '\'' +
                ", title='" + title + '\'' +
                ", year=" + year +
                ", isbn='" + isbn + '\'' +
                '}';
    }
}
