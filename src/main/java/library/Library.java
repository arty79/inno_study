package library;

import java.util.Date;
import java.util.UUID;

import com.loader.Book;
import library.modules.BookInstance;
import library.modules.Booking;
import library.modules.Reader;
import org.apache.log4j.Logger;

import java.util.HashSet;
import java.util.Set;

public class Library {
    private Set<Book> catalog;
    private Set<BookInstance> store;
    private Set<Reader> readers;
    private Set<Booking> bookings;

    static{
        //DOMConfigurator.configure("c:\\Workspace\\inno_study\\src\\log4j.xml");
    }

    private final static Logger LOGGER = Logger.getLogger(Library.class);

    public Library() {
        catalog = new HashSet<>(1024);
        store = new HashSet<>(4096);
        readers = new HashSet<>(512);
        bookings = new HashSet<>(2048);
        LOGGER.debug("Hello, Library");
    }

    public Set<BookInstance> getStore() {
        return new HashSet<>(store);
    }

    public Set<Book> getCatalog() {
        return catalog;
    }

    public Set<Booking> getBookings() {
        return bookings;
    }

    public Set<Reader> getReaders() {
        return readers;
    }

    public void buyBook(String author, String title, String isbn,
                        int quantity, int year) {
        Book newBook = new Book(author, title, year, isbn);
        catalog.add(newBook);
        for (int i = 0; i < quantity; i++) {
            BookInstance bookInstance = new BookInstance(newBook, UUID.randomUUID());
            store.add(bookInstance);
        }
    }


    public void takeBook(String firstName, String secondName, String lastname, long passportNumber,
                         String title) {
        Object[] reader = readers.stream().
                filter((r) -> r.getPassportNumber() == passportNumber).toArray();
        Reader tempReader = null;
        if (reader.length != 0) {
            tempReader = (Reader) reader[0];
        } else {
            tempReader = new Reader(firstName, secondName, lastname, passportNumber);
            readers.add(tempReader);
        }

        BookInstance bookInstance = (BookInstance) store.stream().
                filter((s) -> s.getBook().getTitle().equals(title)).toArray()[0];
        if (bookInstance == null) {
            System.out.println("no such book");
            return;
        }
        Booking booking = new Booking(bookInstance, tempReader, new Date(), new Date());
        bookings.add(booking);
        store.remove(bookInstance);

    }

    public void returnBook(String firstName, String secondName, String lastname, long passportNumber,
                           String title) {
        Reader reader = new Reader(firstName, secondName, lastname, passportNumber);
        Booking booking = (Booking) bookings.stream().
                filter((b) -> b.getBookInstance().getBook().getTitle().equals(title) &&
                        b.getReader().equals(reader)).toArray()[0];
        if (booking == null) {
            System.out.println("no such booking");
            return;
        }
        store.add(booking.getBookInstance());
        bookings.remove(booking);
    }

    public void showAllDate() {
        catalog.forEach(System.out::println);
        System.out.println();
        bookings.forEach(System.out::println);
        System.out.println();
        store.forEach(System.out::println);
    }
}
