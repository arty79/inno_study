package lab_01;

import javafx.util.Pair;
import lab_01.utils.DirectoryReader;
import lab_01.utils.ThreadReader;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {

    private AtomicInteger finalResult = new AtomicInteger();
    private static volatile Boolean isContinue = true;
    private static List<Future<Pair<Integer, String>>> tasks = new ArrayList<>();
    private static File[] files = null;

    public AtomicInteger getFinalResult() {
        return finalResult;
    }

    public Boolean isContinue() {
        return isContinue;
    }

    public static void setIsContinue(Boolean isContinue) {
        Main.isContinue = isContinue;
    }

    public static List<Future<Pair<Integer, String>>> getTasks() {
        return tasks;
    }

    public static File[] getFiles() {
        return files;
    }

    public static void main(String[] args) {

/*        path to directory with files
        String directoryPath = "C:\\Workspace\\inno_study\\src\\lab_01\\TestData";

        generate and write test data into files of directoryPath
        TestWriter.writeTestDataToDirectory(directoryPath);
*/
        String directoryPath;
        Date startDate = new Date();
        Main main = new Main();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            System.out.println("Введите исходную директорию для поиска файлов:");
            directoryPath = reader.readLine();
            File directory = new File(directoryPath);
            if (directory.exists() && directory.isDirectory()) {
                files = DirectoryReader.readDirectory(directory);
            } else {
                System.out.println("По указанному пути не удалось найти директорию");
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Ошибка ввода-вывода");
        }
        if (files != null) {
            main.createThreadPool(main, files);
        } else {
            System.out.println("Нет файлов для обработки.");
        }
        System.out.println("Complete for: " +
                (new Date().getTime() - startDate.getTime()) + " ms");
    }

    public void createThreadPool(Main main, File[] files) {
        int threadCounter = 0;
        ExecutorService service = Executors.newFixedThreadPool(files.length);
        for (File file : files) {
            if (!file.isFile()) {
                continue;
            }
            tasks.add(service.submit(() -> new ThreadReader().readFileByThread(main, file)));
        }
        for (Future<Pair<Integer, String>> task : tasks) {
            try {
                if (!isContinue) {
                    service.shutdownNow();
                }
                threadCounter++;
                System.out.println("Поток: " + threadCounter +
                        " завершен. Файл " + task.get().getValue());
                System.out.println("Количество обработанных чисел:" +
                        task.get().getKey());
                System.out.println();
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.out.println("Выполнение потока прервано");
            } catch (ExecutionException e) {
                e.printStackTrace();
                System.out.println("Ошибка во время выполнения потока");
            }
        }
        service.shutdown();
        try {
            service.awaitTermination(10, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("Выполнение потока прервано");
        }
    }

}
