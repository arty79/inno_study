package lab_01.test;

import lab_01.utils.Parser;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by user on 10.04.2017.
 */
public class ParserTest {

    @Test
    public void isValidNumber() {
        String test1 = "12345";
        String test2 = "-12345";
        String test3 = "12-345";
        String test4 = "*12345";
        assertEquals(true, Parser.isValidNumber(test1));
        assertEquals(true, Parser.isValidNumber(test2));
        assertNotEquals(true, Parser.isValidNumber(test3));
        assertNotEquals(true, Parser.isValidNumber(test4));
    }

    @Test
    public void isEven(){
        String test1 = "2";
        String test2 = "57";
        assertEquals(true, Parser.isEven(test1));
        assertNotEquals(true, Parser.isEven(test2));
    }

    @Test
    public void isPositive(){
        String test1 = "2";
        String test2 = "-57";
        assertEquals(true, Parser.isPositive(test1));
        assertNotEquals(true, Parser.isPositive(test2));
    }
}