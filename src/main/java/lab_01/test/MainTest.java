package lab_01.test;

import lab_01.Main;
import lab_01.utils.TestWriter;
import org.junit.Before;
import org.junit.Test;


import java.io.File;
import java.io.FileReader;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MainTest {

    private static Main main;

    @Before
    public void init() {
        main = new Main();
    }

    @Test
    public void createThreadPool() throws Exception {
        File[] files = main.getFiles();
        files = new File[3];
        for (int i = 0; i < files.length; i++) {
            File file = File.createTempFile("Temp_", "_file.txt");
            TestWriter.writeFile(file);
            files[i] = file;
        }
        main.createThreadPool(main, files);
        assertEquals(3, main.getTasks().size());
        for (int i = 0; i < files.length; i++) {
            FileReader reader = new FileReader(files[i]);
            Scanner sc = new Scanner(reader);
            String str = sc.nextLine();
            String[] numbers = str.split(" ");
            for (int j = 0; j < main.getTasks().size(); j++) {
                if (files[i].getName().equals(main.getTasks().get(j).get().getValue())) {
                    assertTrue(numbers.length == main.getTasks().get(j).get().getKey());
                }
            }
        }
    }


}
