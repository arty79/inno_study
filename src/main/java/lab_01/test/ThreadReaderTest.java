package lab_01.test;

import lab_01.Main;
import lab_01.utils.ProxyThreadable;
import lab_01.utils.ThreadReader;
import lab_01.utils.ThreadReaderProxy;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Proxy;

import static org.junit.Assert.*;

public class ThreadReaderTest {


    private ThreadReader threadReader;
    private Main main;

    @Before
    public void init() {
        main = new Main();
        threadReader = new ThreadReader();
    }

    @Test()
    public void readFileByThread() throws Exception {
        File file = File.createTempFile("Temp_", "_file.txt");
        try (FileWriter writer = new FileWriter(file)) {
            for (int i = 0; i < 10; i++) {
                String s = Integer.toString(i);
                writer.write(s);
                writer.write(" ");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        threadReader.readFileByThread(main, file);
        assertEquals(20, main.getFinalResult().get());
    }

    @Test()
    public void readWrongFileByThread() throws Exception {
        Boolean startFlag = main.isContinue();
        File file = new File("C:\\Workspace\\inno_study\\src\\lab_01\\TestData\\log.txt");
        threadReader.readFileByThread(main, file);
        Boolean finishFlag = main.isContinue();
        assertNotEquals(startFlag, finishFlag);
    }

    @Test()
    public void readWrongFileByThreadWithProxy() throws Exception {
        Boolean startFlag = main.isContinue();
        Boolean finishFlag = true;
        File file = new File("C:\\Workspace\\inno_study\\src\\lab_01\\TestData\\text.txt");
        threadReader.readFileByThread(main, file);
        finishFlag = main.isContinue();
        assertEquals(startFlag, finishFlag);
        ProxyThreadable threadReaderProxy = (ProxyThreadable) Proxy.newProxyInstance(
                ThreadReader.class.getClassLoader(),
                ThreadReader.class.getInterfaces(),
                new ThreadReaderProxy());
        threadReaderProxy.readFileByThread(main, file);
        finishFlag = main.isContinue();
        assertNotEquals(startFlag, finishFlag);
    }
    @After
    public void clearAll() {
        main = new Main();
        threadReader = new ThreadReader();
    }
}
