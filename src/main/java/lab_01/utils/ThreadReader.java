package lab_01.utils;

import javafx.util.Pair;
import lab_01.Main;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by user on 10.04.2017.
 */
public class ThreadReader implements ProxyThreadable{
    public Pair<Integer, String> readFileByThread(Main main, File file) {
        int count = 0;
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            main.setIsContinue(false);
            e.printStackTrace();
            System.out.println("Ошибка ввода-вывода");
        }
        while (scanner.hasNext() && main.isContinue()) {
            String str = scanner.next();
            if (!Parser.isValidNumber(str)) {
                main.setIsContinue(false);
                System.out.println("Файл " + file.getName() + " содержит невалидные символы");
                break;
            }
            if (!Parser.isPositive(str)) {
                continue;
            }
            if (Parser.isEven(str)) {
                System.out.println("Итоговая сумма четных чисел: " +
                        main.getFinalResult().addAndGet(Integer.valueOf(str)));
            }
            count++;
        }
        return new Pair(count, file.getName());
    }
}
