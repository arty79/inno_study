package lab_01.utils;

import java.io.File;

/**
 * Created by user on 10.04.2017.
 */
public class DirectoryReader {

    public static File[] readDirectory(File directory) {
        File[] files = directory.listFiles();
        if (files != null) {
            System.out.println("Количество файлов для обработки: " + files.length);
            System.out.println();
        }
        return files;
    }
}
