package lab_01.utils;

import javafx.util.Pair;
import lab_01.Main;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Scanner;

/**
 * Created by user on 10.04.2017.
 */
public class ThreadReaderProxy implements InvocationHandler{

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if(method.getName().equals("readFileByThread")){
            Field field = args[0].getClass().getDeclaredField("isContinue");
            field.setAccessible(true);
            field.set(args[0].getClass(), false);
            System.out.println(field.get(args[0].getClass()));
        }
        return new Pair(0, "");
    }
}
