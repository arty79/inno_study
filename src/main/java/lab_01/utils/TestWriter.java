package lab_01.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

/**
 * Created by user on 09.04.2017.
 */
public class TestWriter {

    public static void writeTestDataToFiles(File[] files) {
        for (File file : files) {
            writeFile(file);
        }
    }

    public static void writeTestDataToDirectory(String directoryPath) {
        String name;
        for (int i = 0; i < 100; i++) {
            double rand = Math.random() * 100;
            name = Double.toString(rand).replace(".", "") + "_file.txt";
            File file = new File(directoryPath, name);
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            writeFile(file);
        }
    }

    public static void writeFile(File file) {
        int[] ints;
        Random r = new Random();
        int quantity = (int) (Math.random() * 100);
        ints = r.ints(quantity, -50, 100).toArray();
        try (FileWriter writer = new FileWriter(file)) {
            for (int i = 0; i < ints.length; i++) {
                String s = Integer.toString(ints[i]);
                writer.write(s);
                writer.write(" ");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
