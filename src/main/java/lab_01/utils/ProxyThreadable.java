package lab_01.utils;

import javafx.util.Pair;
import lab_01.Main;

import java.io.File;

/**
 * Created by user on 13.04.2017.
 */
public interface ProxyThreadable {
    Pair<Integer, String> readFileByThread(Main main, File file);
}
