package lab_01.utils;

/**
 * Created by user on 10.04.2017.
 */
public class Parser {

    public static boolean isValidNumber(String number) {
        if (number.startsWith("-")) {
            number = number.replace("-", "");
        }
        char[] chars = number.toCharArray();
        for (char ch : chars) {
            if (!Character.isDigit(ch)) {
                return false;
            }
        }
        return true;
    }

    public static boolean isEven(String str) {
        return Integer.valueOf(str) % 2 == 0;
    }

    public static boolean isPositive(String str) {
        return str.startsWith("-") ? false : true;
    }
}
