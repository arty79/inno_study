package lessons05.semaphore;

/**
 * Created by admin on 07.04.2017.
 */
public class Simpler extends Thread {
    public int c;

    public Simpler(int c) {
        this.c = c;
    }

    @Override
    public void run() {
        Consumer.sum(0, 0, c);
    }

    @Override
    public String toString() {
        return "simpler";
    }
}
