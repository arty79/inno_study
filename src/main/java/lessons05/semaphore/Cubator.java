package lessons05.semaphore;

/**
 * Created by admin on 07.04.2017.
 */
public class Cubator extends Thread {

    private int a;

    public Cubator(int a) {
        this.a = a;
    }

    @Override
    public void run() {
        Consumer.sum(a * a * a, 0, 0);
    }

    @Override
    public String toString() {
        return "cub";
    }
}
