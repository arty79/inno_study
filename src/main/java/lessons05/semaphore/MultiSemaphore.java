package lessons05.semaphore;

/**
 * Created by user on 07.04.2017.
 */
public class MultiSemaphore {

    boolean taken = false;

    public synchronized void take() {
        this.taken = true;
        this.notify();
    }

    public synchronized void release() throws InterruptedException {
        while (!this.taken) wait();
        this.taken = false;
    }

}
