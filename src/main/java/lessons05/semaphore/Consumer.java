package lessons05.semaphore;

import java.util.Map;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by admin on 07.04.2017.
 */
public class Consumer {

    public static int result;

    public static int a[] = IntStream.range(0, 3).toArray();
    public static int b[] = IntStream.range(3, 6).toArray();
    public static int c[] = IntStream.range(6, 9).toArray();

    public static Boolean lockCub = false;
    public static Boolean lockQuad = false;
    public static Boolean lockSimple = false;


    public static void main(String[] args) {
        for (int i = 0; i < a.length; i++) {
            Cubator cub = new Cubator(a[i]);
            cub.start();
        }

        for (int i = 0; i < b.length; i++) {
            Quadrator quadro = new Quadrator(b[i]);
            quadro.start();
        }

        for (int i = 0; i < c.length; i++) {
            Simpler simpler = new Simpler(c[i]);
            simpler.start();
        }
    }

    public static void sum(int cube, int quadro, int simple) {
        Thread t = Thread.currentThread();
        if (t.toString().equals("cub")) {
            synchronized (lockCub) {
                result += cube;
                System.out.println("cub " + t.getName());
            }
        }

        if (t.toString().equals("quadro")) {
            synchronized (lockQuad) {
                result += quadro;
                System.out.println("quadro " + t.getName());
            }
        }
        if (t.toString().equals("simpler")) {
            synchronized (lockSimple) {
                lockSimple = true;
                result += simple;
                System.out.println("simpler " + t.getName());
                lockSimple = false;
            }
        }
        System.out.println(result);
        System.out.println();
    }
}
