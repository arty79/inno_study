package lessons05.semaphore;

/**
 * Created by admin on 07.04.2017.
 */
public class Quadrator extends Thread{
    public int b;

    public Quadrator(int b) {
        this.b = b;
    }

    @Override
    public void run() {
        Consumer.sum(0,b*b,0);
    }

    @Override
    public String toString() {
        return "quadro";
    }
}
