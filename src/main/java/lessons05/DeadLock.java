package lessons05;


/**
 * Created by admin on 06.04.2017.
 */
public class DeadLock {

    public static String A = "asasa";
    public static String B = "qwqwq";

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread1();
        Thread t2 = new Thread2();
        t1.start();
        t2.start();

    }

    public static void writeAB() {
        synchronized (DeadLock.B) {
            System.out.println("thread1_B");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
            synchronized (DeadLock.A) {
                System.out.println("thread1_A");
            }
        }
    }

    public static void writeBA() {
        synchronized (DeadLock.A) {
            System.out.println("thread2_A");
            synchronized (DeadLock.B) {
                System.out.println("thread2_B");
            }
        }
    }
}
