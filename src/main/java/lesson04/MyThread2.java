package lesson04;


import control.RandomSecundomer;

/**
 * Created by admin on 06.04.2017.
 */
public class MyThread2 extends Thread {

    private RandomSecundomer timer;

    public MyThread2(RandomSecundomer t) {
        this.timer = t;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (timer) {
                try {
                    if (timer.getTime() % 7 == 0 && timer.getTime() != 0) System.out.println("tic 7sec");
                    timer.wait();
                } catch (InterruptedException e) {
                }
            }
        }
    }
}
