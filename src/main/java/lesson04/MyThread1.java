package lesson04;

/**
 * Created by admin on 06.04.2017.
 */
public class MyThread1 extends Thread {

    private MyTimer timer;

    public MyThread1(MyTimer t) {
        this.timer = t;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (timer) {
                try {
                    if (timer.getTime() % 5 == 0 && timer.getTime() != 0) {
                        System.out.println("tic 5sec");
                    }
                    timer.wait();
                } catch (InterruptedException e) {
                }
            }
        }
    }
}
