package lesson04;

import java.util.Date;

/**
 * Created by admin on 06.04.2017.
 */
public class MyTimer extends Thread {

    private static Date startDate = new Date();
    private static long time;

    public Date getStartDate() {
        return startDate;
    }

    public synchronized static long getTime() {
        return time;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (this) {
                time = (new Date().getTime() - startDate.getTime()) / 1000;
                notifyAll();
                System.out.println(getTime());
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
