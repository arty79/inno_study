package com.chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Artem Panasyuk on 17.04.2017.
 */
public class Main {
    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(5555);
            Socket socket = serverSocket.accept();
            String message = null;
            BufferedReader bufferedReader =
                    new BufferedReader(new InputStreamReader(socket.getInputStream()));
            while((message= bufferedReader.readLine()) != null){
                System.out.println(message);
            }
            socket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
