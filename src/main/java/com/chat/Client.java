package com.chat;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Artem Panasyuk on 17.04.2017.
 */
public class Client {
    public static void main(String[] args) {
        try {
            Socket socket = new Socket("localhost", 5555);
            String message = null;
            BufferedWriter bufferedWriter =
                    new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            bufferedWriter.write("Hello, world!");
            bufferedWriter.close();
            socket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
