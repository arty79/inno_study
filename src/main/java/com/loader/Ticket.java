package com.loader;

/**
 * Created by Artem Panasyuk on 17.04.2017.
 */
public class Ticket {
    private int number;
    private int userId;

    public Ticket() {
    }

    public Ticket(int number, int userId) {
        this.number = number;
        this.userId = userId;
    }

    /**
     * Getter for property 'number'.
     *
     * @return Value for property 'number'.
     */
    public int getNumber() {
        return number;
    }

    /**
     * Setter for property 'number'.
     *
     * @param number Value to set for property 'number'.
     */
    public void setNumber(int number) {
        this.number = number;
    }

    /**
     * Getter for property 'userId'.
     *
     * @return Value for property 'userId'.
     */
    public int getUserId() {
        return userId;
    }

    /**
     * Setter for property 'userId'.
     *
     * @param userId Value to set for property 'userId'.
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }
}
