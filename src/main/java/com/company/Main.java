package com.company;

public class Main {

    public static void main(String[] args) {
        MyList<String> strings = new MyList<>();
        strings.add("qww");
        strings.add("asd");
        strings.add("dfg");
        strings.add("rty");
        printList(strings);
        System.out.println();
        strings.remove("rty");
        printList(strings);
    }
    public static <T> void printList(MyList<T> strings){
        for (T str: strings) {
            System.out.println(str);
        }
    }

}
